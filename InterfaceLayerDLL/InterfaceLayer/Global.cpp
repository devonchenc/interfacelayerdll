#include "Global.h"

#include <sstream>
#include <iomanip>
#include <chrono>

std::string getDataTime(int second)
{
    using namespace std::chrono;
    seconds tp_seconds(second);
    system_clock::time_point tp(tp_seconds);

    time_t tt = system_clock::to_time_t(tp);
    tm* time_tm = localtime(&tt);

    std::ostringstream oss;
    oss << time_tm->tm_year + 1900 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mon + 1 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mday << " ";
    oss << std::setfill('0') << std::setw(2) << time_tm->tm_hour << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_min << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_sec;

    return oss.str();
}
