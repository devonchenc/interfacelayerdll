#pragma once

#include <chrono>
#include <mutex>
#include <queue>
#include <assert.h>

template <class Type>
class SafeQueue
{
public:
    /// Pushes a new value to the back of the `Queue` and notifies one thread on
    /// the waiting side about this event.
    void push(Type value)
    {
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _queue.push(std::move(value));
        }
        _cv.notify_one();
    }

    /// Blocks until at least one element is ready to be popped from the front of
    /// the queue. An optional `timeout` in seconds can be used to limit the time
    /// spent waiting for an element. If the wait times out, an exception is
    /// raised.
    Type pop(std::chrono::milliseconds timeout = std::chrono::milliseconds(0))
    {
        std::unique_lock<std::mutex> lock(_mutex);
        if (timeout.count() > 0)
        {
            if (!_cv.wait_for(lock, timeout, [this] { return !_queue.empty(); }))
            {
                // clang-format off
                //AT_ERROR(
                //    "Timeout in DataLoader queue while waiting for next batch"
                //    " (timeout was ", timeout->count(), " ms)");
                // clang-format on
            }
        }
        else
        {
            _cv.wait(lock, [this] { return !_queue.empty(); });
        }
        assert(!_queue.empty());
        Type value = _queue.front();
        _queue.pop();
        lock.unlock();
        return value;
    }

    /// Empties the queue and returns the number of elements that were present at
    /// the start of the function. No threads are notified about this event as it
    /// is assumed to be used to drain the queue during shutdown of a
    /// `DataLoader`.
    size_t clear()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        const auto size = _queue.size();
        while (!_queue.empty())
        {
            _queue.pop();
        }
        return size;
    }

    bool empty() const
    {
        return _queue.empty();
    }

    size_t size() const
    {
        return _queue.size();
    }

private:
    std::queue<Type> _queue;
    std::mutex _mutex;
    std::condition_variable _cv;
};