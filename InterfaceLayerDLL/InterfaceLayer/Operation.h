#pragma once

#include <vector>
#include <memory>
#include <functional>

#include "Global.h"

class INTERFACELAYER_API Operation
{
public:
    Operation();
    virtual ~Operation() {}

    void setDelayTime(int time) { _delayTime = time; }
    int delayTime() const { return _delayTime; }

    void execute();

    uint16_t reply();

    void setLoop(bool val) { _isLoop = val; }
    bool isLoop() const { return _isLoop; }

protected:
    void cancel() { _isCanceled = true; }

    virtual void onExecute() = 0;
    virtual void onReply() {};

protected:
    uint16_t _errorCode;

private:
    int _delayTime;

    bool _isLoop;

    bool _isCanceled;
};

typedef std::shared_ptr<Operation> OperationPtr;
typedef std::function<void(void)> CallbackVoid;
typedef std::function<void(int)> CallbackInt;
typedef std::function<void(bool)> CallbackBool;
typedef std::function<void(float)> CallbackFloat;
typedef std::function<void(long, long, long)> CallbackLong;
typedef std::function<void(std::vector<std::string>)> CallbackStringVector;
typedef std::function<void(IL_DEVICE_STATUS&)> CallbackDeviceStatus;
