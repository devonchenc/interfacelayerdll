﻿#include "DomesticBoard.h"

#include <functional>
#include <string>
#include "DomesticBoardOpt.h"

bool DomesticBoard::open()
{
    Board::open();

    std::string comPort = "COM5";
    int baudrate = 1000000;
    
    COMMTIMEOUTS timeOut;
	timeOut.ReadIntervalTimeout = MAXDWORD;
	timeOut.ReadTotalTimeoutMultiplier = 0;
	timeOut.ReadTotalTimeoutConstant = 2000;
	timeOut.WriteTotalTimeoutMultiplier = 50;
	timeOut.WriteTotalTimeoutConstant = 2000;

    bool result = _serialPort.initPort(comPort, baudrate, 1, 8, 1, &timeOut);
    if (result)
    {
        onOpen();
    }
    else
    {
        printf("Init COM error.\n");

        _serialPort.closePort();
    }
    return result;
}

void DomesticBoard::close()
{
    Board::close();

    _serialPort.closePort();
}

void DomesticBoard::update(int delta)
{
    Board::update(delta);
}

uint16_t DomesticBoard::readDeviceInfo(IL_INFO_TYPE infoType, std::vector<std::string>& infoVector)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadDeviceInfoOpt(&_serialPort, infoType, infoVector)));
}

uint16_t DomesticBoard::changeBaudrate(int baudrate)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ChangeBaudrateOpt(&_serialPort, baudrate)));
}

uint16_t DomesticBoard::comTest()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ComTestOpt(&_serialPort)));
}

uint16_t DomesticBoard::readStatus(IL_DEVICE_STATUS& deviceStatus)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadStatusOpt(&_serialPort, deviceStatus)));
}

uint16_t DomesticBoard::clearAllError()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ClearAllErrorOpt(&_serialPort)));
}

uint16_t DomesticBoard::syncTime()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new SyncTimeOpt(&_serialPort)));
}

uint16_t DomesticBoard::readTime(int& readTime, bool compare)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    uint16_t result = invoke(OperationPtr(new ReadTimeOpt(&_serialPort, readTime)));

    if (compare)
    {
        compareTime(readTime);
    }

    return result;
}

uint16_t DomesticBoard::writeFIRParameter(const std::vector<int>& value)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new WriteFIRParameterOpt(&_serialPort, value)));
}

uint16_t DomesticBoard::writeCICParameter(uint16_t value)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new WriteCICParameterOpt(&_serialPort, value)));
}

uint16_t DomesticBoard::writeTimeCoreParameter(int address, const std::vector<TimeCore>& parameters)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new WriteTimeCoreParameterOpt(&_serialPort, address, parameters)));
}

uint16_t DomesticBoard::readParameter()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadParameterOpt(&_serialPort)));
}

uint16_t DomesticBoard::startAcquire()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new StartAcquireOpt(&_serialPort)));
}

uint16_t DomesticBoard::readAcquiredData(int& readLen, uint16_t* dataBuffer)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadAcquiredDataOpt(&_serialPort, readLen, dataBuffer)));
}

uint16_t DomesticBoard::writeRamData(uint64_t ramAddress, int writeLen, const uint16_t* dataBuffer)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new WriteRamDataOpt(&_serialPort, ramAddress, writeLen, dataBuffer)));
}

uint16_t DomesticBoard::readRamData(uint64_t ramAddress, int readLen, uint16_t* dataBuffer)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadRamDataOpt(&_serialPort, ramAddress, readLen, dataBuffer)));
}

uint16_t DomesticBoard::enterDVPMode()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new EnterDVPModeOpt(&_serialPort)));
}

uint16_t DomesticBoard::exitDVPMode()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ExitDVPModeOpt(&_serialPort)));
}

uint16_t DomesticBoard::terminateExe()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new TerminateExeOpt(&_serialPort)));
}

uint16_t DomesticBoard::readAuxInfo(float& temperature)
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new ReadAuxInfoOpt(&_serialPort, temperature)));
}

uint16_t DomesticBoard::enterBSLMode()
{
    if (!_serialPort.isOpened())
        return IL_BOARD_CLOSED;

    return invoke(OperationPtr(new EnterBSLModeOpt(&_serialPort)));
}

bool DomesticBoard::compareTime(int readTime)
{
    using namespace std::chrono;
    system_clock::time_point now = system_clock::now();
    // 获取1970.1.1以来的秒数
    seconds timeSinceEpoch = duration_cast<seconds>(now.time_since_epoch());
    int second = int(timeSinceEpoch.count());

    if (abs(readTime - second) > 1)
    {
        DEBUG_STDERR("The device time does not match the system time");
        return false;
    }
    else
    {
        DEBUG_STDOUT("The device time matches the system time");
        return true;
    }
}
