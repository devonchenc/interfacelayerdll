#include "DomesticBoardOpt.h"

#include <iostream>
#include <sstream>
#include <random>
#include <ctime>
#include <chrono>
#include <limits.h>
#include <assert.h>

#define M_PI       3.14159265358979323846   // pi

ReadDeviceInfoOpt::ReadDeviceInfoOpt(SerialPortImpl* comHandle, IL_INFO_TYPE infoType, std::vector<std::string>& infoVector)
    : SerialPortOpt(INSB_READ_DEVICE_INFO, 1, comHandle)
    , _infoType(infoType)
    , _infoVector(infoVector)
{

}

void ReadDeviceInfoOpt::onExecute()
{
    DEBUG_STDOUT("[Read Device Info]");

    switch (_infoType)
    {
    case DeviceType:
    {
        DEBUG_STDOUT("Mode = Instrument model");
    }
    break;
    case HardwareVersion:
    {
        DEBUG_STDOUT("Mode = Control circuit hardware version number");
    }
    break;
    case SoftwareVersion:
    {
        DEBUG_STDOUT("Mode = Control circuit software version number");
    }
    break;
    case RAMVersion:
    {
        DEBUG_STDOUT("Mode = RAM version number");
    }
    break;
    }

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = _infoType;

    sendAndReceive();
}

bool ReadDeviceInfoOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Device Info = Fail, Error Code = " + oss.str());
        return false;
    }

    _infoVector.clear();

    uint16_t* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;

    switch (_infoType)
    {
    case DeviceType:
    {
        uint16_t deviceTypeLength = *(buffer++);
        std::string info = extractString(buffer, deviceTypeLength);
        _infoVector.push_back(info);

        DEBUG_STDOUT("DeviceTpye = " + info);
    }
    break;
    case HardwareVersion:
    {
        uint16_t comBoardVersionLength = *(buffer++);
        std::string comBoardVersion = extractString(buffer, comBoardVersionLength);
        _infoVector.push_back(comBoardVersion);

        DEBUG_STDOUT("ComBoardVersion = " + comBoardVersion);

        buffer += comBoardVersionLength;
        uint16_t motherBoardVersionLength = *(buffer++);
        std::string motherBoardVersion = extractString(buffer, motherBoardVersionLength);
        _infoVector.push_back(motherBoardVersion);

        DEBUG_STDOUT("MotherBoardVersion = " + motherBoardVersion);

        buffer += motherBoardVersionLength;
        uint16_t coreBoardVersionLength = *(buffer++);
        std::string coreBoardVersion = extractString(buffer, coreBoardVersionLength);
        _infoVector.push_back(coreBoardVersion);

        DEBUG_STDOUT("CoreBoardVersion = " + coreBoardVersion);
    }
    break;
    case SoftwareVersion:
    {
        uint16_t comBoardVersionLength = *(buffer++);
        std::string comBoardVersion = extractString(buffer, comBoardVersionLength);
        _infoVector.push_back(comBoardVersion);

        DEBUG_STDOUT("ComBoardVersion = " + comBoardVersion);

        buffer += comBoardVersionLength;
        uint16_t coreBoardVersionLength = *(buffer++);
        std::string coreBoardVersion = extractString(buffer, coreBoardVersionLength);
        _infoVector.push_back(coreBoardVersion);

        DEBUG_STDOUT("CoreBoardVersion = " + coreBoardVersion);
    }
    break;
    case RAMVersion:
    {
        uint16_t ramVersionLength = *(buffer++);
        uint16_t ramType = *(buffer++);
        uint16_t storageDepth1 = *(buffer++);
        uint16_t storageDepth2 = *(buffer++);
        uint16_t bitWdith = *(buffer++);
        std::string ramID = extractString(buffer, ramVersionLength - 4);

        std::ostringstream oss;
        oss << "RAM Type = ";
        switch (ramType)
        {
        case 1:
            oss << "DDR";
            break;
        case 2:
            oss << "DDR II";
            break;
        case 3:
            oss << "DDR III";
            break;
        case 4:
            oss << "DDR IV";
            break;
        case 5:
            oss << "DDR V";
            break;
        case 6:
            oss << "SRAM";
            break;
        case 7:
            oss << "DSRAM";
            break;
        default:
            oss << "UNKNOWN";
            break;
        }
        oss << ", StorageDepth = " << storageDepth1 << storageDepth2 << ", BitWidth = " << bitWdith << ", RAM ID = " << ramID;
        _infoVector.push_back(oss.str());
        DEBUG_STDOUT(oss.str());
    }
    break;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

ChangeBaudrateOpt::ChangeBaudrateOpt(SerialPortImpl* comHandle, int baudrate)
    : SerialPortOpt(INSB_CHANGE_BAUDRATE, 2, comHandle)
    , _baudrate(baudrate)
{

}

void ChangeBaudrateOpt::onExecute()
{
    DEBUG_STDOUT("[Change Baudrate]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0;
    _instructionBuffer[3] = _baudrate / 100;

    sendAndReceive();
}

bool ChangeBaudrateOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Change Baudrate = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    uint16_t changeFlag = *(buffer++);        // 1.设备接收并改变波特率, 2.设备不改变波特率
    uint16_t receivedBaudrateHigh = *(buffer++);  // 接收到的波特率
    uint16_t receivedBaudrateLow = *(buffer++);
    uint16_t newBaudrateHigh = *(buffer++);      // 设备新的波特率
    uint16_t newBaudrateLow = *(buffer++);
    int receivedBaudrate = int(receivedBaudrateHigh << 16) | int(receivedBaudrateLow);
    int newBaudrate = int(newBaudrateHigh << 16) | int(newBaudrateLow);

    std::ostringstream oss;
    oss << "Change Flag = ";
    if (changeFlag == 1)
    {
        oss << "true";
    }
    else if (changeFlag == 2)
    {
        oss << "false";
    }
    else
    {
        oss << "unknown";
    }
    oss << ", ReceivedBaudrate = " << receivedBaudrate * 100;
    oss << ", NewBaudrate = " << newBaudrate * 100;

    DEBUG_STDOUT(oss.str());

    return true;
}

//////////////////////////////////////////////////////////////////////////

ComTestOpt::ComTestOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_COM_TEST, 0, comHandle)
{

}

void ComTestOpt::onExecute()
{
    DEBUG_STDOUT("[COM Test]");

    // 产生随机数作为数据长度(50-400)
    std::default_random_engine e(static_cast<unsigned int>(time(nullptr)));
    std::uniform_int_distribution<int> distribInt(50, 400);
    _sendDataLen = distribInt(e);

    // 重新分配内存
    allocateBuffer();

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    // 随机填充
    std::uniform_int_distribution<uint16_t> distribShortInt(0, USHRT_MAX);
    for (int i = 0; i < _sendDataLen; i++)
    {
        _instructionBuffer[i + INSTRUCTION_DATA_OFFSET] = distribShortInt(e);
    }

    sendAndReceive();
}

bool ComTestOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("COM Test = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    // Compare received random data
    for (int i = 0; i < _responseDataLen - RESPONSE_DATA_OFFSET - 1; i++)
    {
        if (buffer[i] != _instructionBuffer[i + INSTRUCTION_DATA_OFFSET])
        {
            DEBUG_STDERR("COM Test = Fail, the received data doesn't match the sent data");
            return false;
        }
    }

    DEBUG_STDOUT("COM Test Success");

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadStatusOpt::ReadStatusOpt(SerialPortImpl* comHandle, IL_DEVICE_STATUS& deviceStatus)
    : SerialPortOpt(INSB_READ_STATUS, 0, comHandle)
    , _statusWordHigh(0)
    , _statusWordLow(0)
    , _deviceStatus(deviceStatus)
{

}

void ReadStatusOpt::onExecute()
{
    DEBUG_STDOUT("[Read Status Word]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

bool ReadStatusOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Status = Fail, Error Code = " + oss.str());
        return false;
    }

    _statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    _statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = _statusWordHigh;
    uint16_t FPGAState = _statusWordLow & 0x000F;
    uint16_t FPGACfg = (_statusWordLow & 0x0010) >> 4;
    uint16_t FPGAPLL = (_statusWordLow & 0x0020) >> 5;

    std::ostringstream oss;
    _deviceStatus.TopLoopNum = topLoopNum;
    oss << "TopLoopNum = " << topLoopNum << ", FPGA PLL = ";
    _deviceStatus.FPGAPLL = (FPGAPLL == 1);
    if (FPGAPLL == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    _deviceStatus.FPGACfg = (FPGACfg == 1);
    oss << ", FPGA Cfg = ";
    if (FPGACfg == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    _deviceStatus.State = static_cast<IL_FPGA_STATE>(FPGAState);
    oss << ", FPGAState = ";
    switch (FPGAState)
    {
    case 0:
        oss << "idle/none";
        break;
    case 1:
        oss << "transmit";
        break;
    case 2:
        oss << "receive";
        break;
    case 3:
        oss << "transcive";
        break;
    case 4:
        oss << "wait";
        break;
    case 5:
        oss << "rdwrram";
        break;
    case 6:
        oss << "program";
        break;
    default:
        oss << "invalid";
        break;
    }
    DEBUG_STDOUT(oss.str());

    return true;
}

//////////////////////////////////////////////////////////////////////////

ClearAllErrorOpt::ClearAllErrorOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_CLEAR_ALL_ERROR, 1, comHandle)
{

}

void ClearAllErrorOpt::onExecute()
{
    DEBUG_STDOUT("[Clear All Error]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0505;

    sendAndReceive();
}

//////////////////////////////////////////////////////////////////////////

SyncTimeOpt::SyncTimeOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_SYNC_TIME, 2, comHandle)
{

}

void SyncTimeOpt::onExecute()
{
    DEBUG_STDOUT("[Sync Time]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    using namespace std::chrono;
    system_clock::time_point now = system_clock::now();
    // 获取1970.1.1以来的秒数
    seconds timeSinceEpoch = duration_cast<seconds>(now.time_since_epoch());
    int second = int(timeSinceEpoch.count());
    _instructionBuffer[2] = second >> 16;           // Timestamp high word
    _instructionBuffer[3] = second & 0x0000FFFF;    // Timestamp low word

    sendAndReceive();
}

bool SyncTimeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Sync Time = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Sync Time = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadTimeOpt::ReadTimeOpt(SerialPortImpl* comHandle, int& readTime)
    : SerialPortOpt(INSB_READ_TIME, 0, comHandle)
    , _readTime(readTime)
{

}

void ReadTimeOpt::onExecute()
{
    DEBUG_STDOUT("[Read Time]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

bool ReadTimeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Time = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t timeStampHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    uint16_t timeStampLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];
    _readTime = int(timeStampHigh << 16) | int(timeStampLow);

    std::ostringstream oss;
    oss << "TimeValue = " << time;
    DEBUG_STDOUT(oss.str());

    DEBUG_STDOUT("Time = " + getDataTime(_readTime));

    return true;
}

//////////////////////////////////////////////////////////////////////////

WriteFIRParameterOpt::WriteFIRParameterOpt(SerialPortImpl* comHandle, const std::vector<int>& value)
    : SerialPortOpt(INSB_WRITE_PARA, 65, comHandle)
    , _value(value)
{
    // FIR滤波器参数最多64个
    assert(_value.size() <= 64);
}

void WriteFIRParameterOpt::onExecute()
{
    DEBUG_STDOUT("[Write FIR Parameter]");

    DEBUG_STDOUT("ParaType = FIR");
    std::ostringstream oss;
    oss << "Parameters = ";
    for (int i = 0; i < _value.size(); i++)
    {
        oss << _value[i];
        if (i < _value.size() - 1)
        {
            oss << ", ";
        }
    }
    DEBUG_STDOUT(oss.str());

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = IL_PARA_TYPE::FIR;

    // 填充FIR滤波器参数
    for (int i = 0; i < _value.size(); i++)
    {
        _instructionBuffer[3 + i] = _value[i];
    }

    sendAndReceive();
}

bool WriteFIRParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDOUT("Write FIR Parameter = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        if (_errorCode == 0x0200)
        {
            _errorCode = IL_WRONG_SEND_DATA;
        }
        DEBUG_STDOUT("Write FIR Parameter = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

WriteCICParameterOpt::WriteCICParameterOpt(SerialPortImpl* comHandle, uint16_t value)
    : SerialPortOpt(INSB_WRITE_PARA, 2, comHandle)
    , _value(value)
{

}

void WriteCICParameterOpt::onExecute()
{
    DEBUG_STDOUT("[Write CIC Parameter]");

    DEBUG_STDOUT("ParaType = CIC");
    std::ostringstream oss;
    oss << "Parameter = " << _value;
    DEBUG_STDOUT(oss.str());

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = IL_PARA_TYPE::CIC;
    _instructionBuffer[3] = _value;

    sendAndReceive();
}

bool WriteCICParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x0200)
        {
            _errorCode = IL_WRONG_SEND_DATA;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDOUT("Write CIC Parameter = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Write CIC Parameter = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

WriteTimeCoreParameterOpt::WriteTimeCoreParameterOpt(SerialPortImpl* comHandle, int address, const std::vector<TimeCore>& parameters)
    : SerialPortOpt(INSB_WRITE_PARA, int(3 + parameters.size() * 4), comHandle)
    , _address(address)
    , _parameters(parameters)
{

}

void WriteTimeCoreParameterOpt::onExecute()
{
    DEBUG_STDOUT("[Write TimeCore Parameter]");

    DEBUG_STDOUT("ParaType = TimeCore");
    for (int i = 0; i < _parameters.size(); i++)
    {
        std::map<std::string, std::string>::iterator iter = _parameters[i].find("TimeCoreType");
        if (iter == _parameters[i].end())
            continue;

        std::ostringstream oss;
        oss << iter->first << " = " << iter->second;

        for (iter = _parameters[i].begin(); iter != _parameters[i].end(); iter++)
        {
            if (iter->first != "TimeCoreType")
            {
                oss << ", " << iter->first << " = " << iter->second;
            }
        }
        DEBUG_STDOUT(oss.str());
    }

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = IL_PARA_TYPE::TIMECORE;
    _instructionBuffer[3] = _address & 0x0000FFFF;   // TimeCore写入地址
    _instructionBuffer[4] = _address << 16;
    // 写入TimeCore
    int bufferOffset = 5;
    for (int i = 0; i < _parameters.size(); i++)
    {
        if (translatedintoCode(_parameters[i], _instructionBuffer + bufferOffset))
        {
            bufferOffset += 4;
        }
    }

    sendAndReceive();
}

bool WriteTimeCoreParameterOpt::translatedintoCode(const TimeCore& timeCore, uint16_t* buffer)
{
    std::map<std::string, std::string>::const_iterator iter = timeCore.find("TimeCoreType");
    if (iter == timeCore.end())
        return false;

    // 时间核指令
    std::string instruction = iter->second;
    if (instruction == "TXS")
    {
        buffer[0] = IL_TIMECORE_TYPE::TXS;
        int dacen = 0;
        float tx_phase = 0;
        float frequency = 0;
        float dac_ratio = 0;
        int load = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "dacen")
            {
                ss >> dacen;
            }
            else if (iter->first == "tx_phase")
            {
                ss >> tx_phase;
            }
            else if (iter->first == "frequency")
            {
                ss >> frequency;
            }
            else if (iter->first == "dac_ratio")
            {
                ss >> dac_ratio;
            }
            else if (iter->first == "load")
            {
                ss >> load;
            }
        }

        // 计算freq_bits
        int freq_bits = int(frequency / 75.0f * (1 << 24));
        int tx_phase_bits = int(tx_phase * (1 << 12) / (2 * M_PI));
        int dac_ration_bits = int(dac_ratio * ((1 << 7) + 1));

        buffer[1] |= dacen & 0b1;                   // dacen
        buffer[1] |= (load & 0b1) << 1;             // load
        buffer[1] |= (tx_phase_bits & 0xFFF) << 4;  // tx_phase[11:0]
        buffer[2] |= dac_ration_bits & 0xFF;        // dac_ration[7:0]
        buffer[2] |= (freq_bits & 0xFF) << 8;       // freq_bits[7:0]
        buffer[3] |= (freq_bits & 0xFFFF00) >> 8;   // freq_bits[23:8]
    }
    else if (instruction == "WAIT")
    {
        buffer[0] = IL_TIMECORE_TYPE::WAIT;
        int delayTime = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "dataT")
            {
                ss >> delayTime;
            }
        }

        // 将时间us转换为周期数
        int dataT = delayTime * 75;

        buffer[0] |= (dataT & 0x000F) << 12;        // dataT[3:0]
        buffer[1] |= (dataT & 0xFFFF0) >> 4;        // dataT[19:4]
    }
    else if (instruction == "LOOPS")
    {
        buffer[0] = IL_TIMECORE_TYPE::LOOPS;
        int rloopsel = 0;
        int dataR = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "Rloopsel")
            {
                ss >> rloopsel;
            }
            else if (iter->first == "dataR")
            {
                ss >> dataR;
            }
        }

        buffer[0] |= (rloopsel & 0x000F) << 4;      // Rloopsel[3:0]
        buffer[0] |= (dataR & 0x000F) << 12;        // dataR[3:0]
        buffer[1] |= (dataR & 0xFFFF0) >> 4;        // dataR[19:4]
    }
    else if (instruction == "LOOPE")
    {
        buffer[0] = IL_TIMECORE_TYPE::LOOPE;
        int rloopsel = 0;
        int addra = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "Rloopsel")
            {
                ss >> rloopsel;
            }
            else if (iter->first == "addra")
            {
                ss >> addra;
            }
        }

        buffer[0] |= (rloopsel & 0x000F) << 4;      // Rloopsel[3:0]
        buffer[1] |= addra & 0xFFFF;                // addra[15:0]
    }
    else if (instruction == "RXE")
    {
        buffer[0] = IL_TIMECORE_TYPE::RXE;
        int rxen0 = 0;
        int rxen1 = 0;
        int rxen2 = 0;
        int rxen3 = 0;
        int rxen4 = 0;
        int rxen5 = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "rxen0")
            {
                ss >> rxen0;
            }
            else if (iter->first == "rxen1")
            {
                ss >> rxen1;
            }
            else if (iter->first == "rxen2")
            {
                ss >> rxen2;
            }
            else if (iter->first == "rxen3")
            {
                ss >> rxen3;
            }
            else if (iter->first == "rxen4")
            {
                ss >> rxen4;
            }
            else if (iter->first == "rxen5")
            {
                ss >> rxen5;
            }
        }

        buffer[2] |= rxen0 & 0b1;                   // rexn[0]
        buffer[2] |= (rxen1 & 0b1) << 1;            // rexn[1]
        buffer[2] |= (rxen2 & 0b1) << 2;            // rexn[2]
        buffer[2] |= (rxen3 & 0b1) << 3;            // rexn[3]
        buffer[2] |= (rxen4 & 0b1) << 4;            // rexn[4]
        buffer[2] |= (rxen5 & 0b1) << 5;            // rexn[5]
    }
    else if (instruction == "OTL")
    {
        buffer[0] = IL_TIMECORE_TYPE::OTL;
        int pamp_ctr = 0;
        int outer_ctrl = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "pamp_ctr")
            {
                ss >> pamp_ctr;
            }
            else if (iter->first == "outer_ctrl")
            {
                ss >> outer_ctrl;
            }
        }

        buffer[2] |= pamp_ctr & 0b1;                // pamp_ctr
        buffer[2] |= (outer_ctrl & 0xF) << 1;       // rexn[1]
    }
    else if (instruction == "RXLP")
    {
        buffer[0] = IL_TIMECORE_TYPE::RXLP;
        int load = 0;
        float frequency = 0;
        float rx_phase = 0;
        int acq_number = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "load")
            {
                ss >> load;
            }
            else if (iter->first == "frequency")
            {
                ss >> frequency;
            }
            else if (iter->first == "rx_phase")
            {
                ss >> rx_phase;
            }
            else if (iter->first == "acq_number")
            {
                ss >> acq_number;
            }
        }

        // 计算freq_bits
        int freq_bits = int(round((frequency / 75.0f) * (1 << 24)));
        int rx_phase_bits = int(round(rx_phase * (1 << 12) / (2 * M_PI)));

        buffer[0] |= (load & 0b1) << 4;             // load
        buffer[0] |= (acq_number & 0xFF) << 8;      // acq_number[7:0]
        buffer[1] |= (acq_number & 0xFFF00) >> 8;   // acq_number[19:8]
        buffer[1] |= (rx_phase_bits & 0xF) << 12;   // rx_phase_bits[3:0]
        buffer[2] |= (rx_phase_bits & 0xFF0) >> 4;  // rx_phase_bits[11:4]
        buffer[2] |= (freq_bits & 0xFF) << 8;       // freq_bits[7:0]
        buffer[3] |= (freq_bits & 0xFFFF00) >> 8;   // freq_bits[23:8]
    }
    else if (instruction == "END")
    {
        buffer[0] = IL_TIMECORE_TYPE::END;
    }
    else
    {
        DEBUG_STDERR("Wrong TimeCore Instruction");
    }

    return true;
}

bool WriteTimeCoreParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x0200)
        {
            _errorCode = IL_WRONG_SEND_DATA;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Write TimeCore Parameter = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Write TimeCore Parameter = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadParameterOpt::ReadParameterOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_READ_PARA, 0, comHandle)
{

}

void ReadParameterOpt::onExecute()
{
    DEBUG_STDOUT("[Read Parameter]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

bool ReadParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Parameter = Fail, Error Code = " + oss.str());
        return false;
    }

    // 解析读取到的参数

    return true;
}

//////////////////////////////////////////////////////////////////////////

StartAcquireOpt::StartAcquireOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_START_ACQUIRE, 1, comHandle)
{

}

void StartAcquireOpt::onExecute()
{
    DEBUG_STDOUT("[Start Acquire]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0A0A;

    sendAndReceive();
}

bool StartAcquireOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_WRONG_SEND_DATA;
        }
        else if (_errorCode == 0x0100)
        {
            _errorCode = IL_READ_DATA_LENGTH_CALIBRATED;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Start Acquire = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Start Acquire = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadAcquiredDataOpt::ReadAcquiredDataOpt(SerialPortImpl* comHandle, int& readLen, uint16_t* dataBuffer)
    : SerialPortOpt(INSB_READ_ACQUIRED_DATA, 0, comHandle)
    , _readLen(readLen)
    , _dataBuffer(dataBuffer)
{
    assert(_dataBuffer);
}

void ReadAcquiredDataOpt::onExecute()
{
    DEBUG_STDOUT("[Read Acquired Data]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

bool ReadAcquiredDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Acquired Data = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t dataLength = *_responseBuffer;
    uint16_t* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    _readLen = dataLength - RESPONSE_DATA_OFFSET - 1;
    memcpy(_dataBuffer, buffer, sizeof(uint16_t) * _readLen);

    DEBUG_STDOUT("Read Acquire = Success");
    return true;
}

//////////////////////////////////////////////////////////////////////////

WriteRamDataOpt::WriteRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int writeLen, const uint16_t* dataBuffer)
    : SerialPortOpt(INSB_WRITE_RAW_DATA, 4 + writeLen, comHandle)
    , _ramAddress(ramAddress)
    , _writeLen(writeLen)
    , _dataBuffer(dataBuffer)
{
    assert(_dataBuffer);
    assert(_writeLen > 0);
    if (_writeLen % 2)
    {
        // 如果是奇数则变为偶数
        _writeLen = _writeLen - 1;
    }
}

void WriteRamDataOpt::onExecute()
{
    DEBUG_STDOUT("[Write RAM Data]");

    std::ostringstream oss;
    oss << "RAM address = " << _ramAddress;
    DEBUG_STDOUT(oss.str());

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = (_ramAddress >> 48) & 0x0000FFFF;
    _instructionBuffer[3] = (_ramAddress >> 32) & 0x0000FFFF;
    _instructionBuffer[4] = (_ramAddress >> 16) & 0x0000FFFF;
    _instructionBuffer[5] = _ramAddress & 0x0000FFFF;
    memcpy(_instructionBuffer + 6, _dataBuffer, sizeof(uint16_t) * _writeLen);

    sendAndReceive();
}

bool WriteRamDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_WRONG_RAW_ADDRESS;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Write RAM Data = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Write RAM Data = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadRamDataOpt::ReadRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int readLen, uint16_t* dataBuffer)
    : SerialPortOpt(INSB_READ_RAW_DATA, 5, comHandle)
    , _ramAddress(ramAddress)
    , _readLen(readLen)
    , _dataBuffer(dataBuffer)
{
    assert(_dataBuffer);
}

void ReadRamDataOpt::onExecute()
{
    DEBUG_STDOUT("[Read RAM Data]");

    std::ostringstream oss;
    oss << "RAM address = " << _ramAddress;
    DEBUG_STDOUT(oss.str());
    oss.str("");
    oss << "ReadDataLength = " << _readLen;
    DEBUG_STDOUT(oss.str());

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = (_ramAddress >> 48) & 0x0000FFFF;
    _instructionBuffer[3] = (_ramAddress >> 32) & 0x0000FFFF;
    _instructionBuffer[4] = (_ramAddress >> 16) & 0x0000FFFF;
    _instructionBuffer[5] = _ramAddress & 0x0000FFFF;
    _instructionBuffer[6] = _readLen;

    sendAndReceive();
}

bool ReadRamDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_WRONG_RAW_ADDRESS;
        }
        else if (_errorCode == 0x0100)
        {
            _errorCode = IL_READ_DATA_LENGTH_CALIBRATED;
        }

        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read RAM Data = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t dataLength = *_responseBuffer;
    uint16_t* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    memcpy(_dataBuffer, buffer, sizeof(uint16_t) * (dataLength - RESPONSE_DATA_OFFSET - 1));

    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterDVPModeOpt::EnterDVPModeOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_ENTER_DVP_MODE, 1, comHandle)
{

}

void EnterDVPModeOpt::onExecute()
{
    DEBUG_STDOUT("[Enter DVP Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0E0E;

    sendAndReceive();
}

bool EnterDVPModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x0100)
        {
            _errorCode = IL_ENTER_DVP_FAILED;
        }
        else if (_errorCode == 0x8000)
        {
            _errorCode = IL_WRONG_RECEIVED_CHECKSUM;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Enter DVP Mode = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Enter DVP Mode = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ExitDVPModeOpt::ExitDVPModeOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_EXIT_DVP_MODE, 1, comHandle)
{

}

void ExitDVPModeOpt::onExecute()
{
    DEBUG_STDOUT("[Exit DVP Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0F0F;

    sendAndReceive();
}

bool ExitDVPModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_EXIT_DVP_FAILED;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Exit DVP Mode = Fail, Error Code = " + oss.str());
        return true;
    }
    else
    {
        DEBUG_STDOUT("Exit DVP Mode = Success");
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

TerminateExeOpt::TerminateExeOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_TEMINATE_EXE, 1, comHandle)
{

}

void TerminateExeOpt::onExecute()
{
    DEBUG_STDOUT("[Terminate Exe]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0101;

    sendAndReceive();
}

bool TerminateExeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Terminate Exe = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    uint16_t statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = statusWordHigh;
    uint16_t FPGAState = statusWordLow & 0x000F;
    uint16_t FPGACfg = (statusWordLow & 0x0010) >> 4;
    uint16_t FPGAPLL = (statusWordLow & 0x0020) >> 5;

    std::ostringstream oss;
    oss << "TopLoopNum = " << topLoopNum << ", FPGA PLL = ";
    if (FPGAPLL == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGA Cfg = ";
    if (FPGACfg == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGAState = ";
    switch (FPGAState)
    {
    case 0:
        oss << "idle/none";
        break;
    case 1:
        oss << "transmit";
        break;
    case 2:
        oss << "receive";
        break;
    case 3:
        oss << "transcive";
        break;
    case 4:
        oss << "wait";
        break;
    case 5:
        oss << "rdwrram";
        break;
    case 6:
        oss << "program";
        break;
    default:
        oss << "invalid";
        break;
    }
    DEBUG_STDOUT(oss.str());

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadAuxInfoOpt::ReadAuxInfoOpt(SerialPortImpl* comHandle, float& temperature)
    : SerialPortOpt(INSB_READ_AUX_INFO, 1, comHandle)
    , _temperature(temperature)
{

}

void ReadAuxInfoOpt::onExecute()
{
    DEBUG_STDOUT("[Read Aux Info]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 1;      // 1:读取单片机温度, 其他值无效

    sendAndReceive();
}

bool ReadAuxInfoOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_UNSUPPORTED_AUX_INFO_TYPE;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Read Aux Info = Fail, Error Code = " + oss.str());
        return false;
    }

    uint16_t auxInfo = _responseBuffer[RESPONSE_DATA_OFFSET];
    _temperature = auxInfo / 10.0f;

    std::ostringstream oss;
    oss << "Temperature = " << _temperature;
    DEBUG_STDOUT(oss.str());

    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterBSLModeOpt::EnterBSLModeOpt(SerialPortImpl* comHandle)
    : SerialPortOpt(INSB_ENTER_BSL_MODE, 1, comHandle)
{

}

void EnterBSLModeOpt::onExecute()
{
    DEBUG_STDOUT("[Enter BSL Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x2828;

    sendAndReceive();
}

bool EnterBSLModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x8000)
        {
            _errorCode = IL_ENTER_BSL_FAILED;
        }
        std::ostringstream oss;
        oss << _errorCode;
        DEBUG_STDERR("Enter BSL Mode = Fail, Error Code = " + oss.str());
        return false;
    }
    else
    {
        DEBUG_STDOUT("Enter BSL Mode = Success");
        return true;
    }
}