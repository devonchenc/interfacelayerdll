#pragma once

#include <iostream>
#include <map>

#include "Global.h"
#include "Operation.h"
#include "Scheduler.h"

typedef std::map<std::string, std::string> TimeCore;

class INTERFACELAYER_API Board
{
public:
    Board() {}
    virtual ~Board()
    {
        _scheduler.stop();
        _scheduler.cancel();
    }

    virtual bool open()
    {
        _scheduler.start();
        return true;
    }

    virtual void close()
    {
        _scheduler.stop();
        _scheduler.cancel();
    }

    virtual bool isOpened() const { return false; }

    virtual void update(int delta)
    {
        _scheduler.update();
    }

    // 读取设备信息
    virtual uint16_t readDeviceInfo(IL_INFO_TYPE infoType, std::vector<std::string>& infoVector) { return IL_OK; }

    // 改变波特率
    virtual uint16_t changeBaudrate(int baudrate) { return IL_OK; }

    // 串口测试
    virtual uint16_t comTest() { return IL_OK; }

    // 读取设备当前状态
    virtual uint16_t readStatus(IL_DEVICE_STATUS& deviceStatus) { return IL_OK; }

    // 清除设备所有可清除的错误
    virtual uint16_t clearAllError() { return IL_OK; }

    // 将当前上位机的时间同步至设备中
    virtual uint16_t syncTime() { return IL_OK; }

    // 读取设备当前时间
    virtual uint16_t readTime(int& readTime, bool compare = false) { return IL_OK; }

    // 写64个FIR滤波器参数
    virtual uint16_t writeFIRParameter(const std::vector<int>& value) { return IL_OK; }

    // 写1个CIC参数
    virtual uint16_t writeCICParameter(uint16_t value) { return IL_OK; }

    // 写时间核代码
    virtual uint16_t writeTimeCoreParameter(int address, const std::vector<TimeCore>& parameters) { return IL_OK; }

    // 读取参数表
    virtual uint16_t readParameter() { return IL_OK; }

    // 开始采集
    virtual uint16_t startAcquire() { return IL_OK; }

    // 读取采集数据
    virtual uint16_t readAcquiredData(int& readLen, uint16_t* dataBuffer) { return IL_OK; }

    // 向设备RAM指定位置写入数据
    virtual uint16_t writeRamData(uint64_t ramAddress, int writeLen, const uint16_t* dataBuffer) { return IL_OK; }

    // 读取RAM指定位置的数据
    virtual uint16_t readRamData(uint64_t ramAddress, int readLen, uint16_t* dataBuffer) { return IL_OK; }

    // 进入DVP开发模式
    virtual uint16_t enterDVPMode() { return IL_OK; }

    // 退出DVP开发模式
    virtual uint16_t exitDVPMode() { return IL_OK; }

    // 放弃当前任务
    virtual uint16_t terminateExe() { return IL_OK; }

    // 读取设备辅助信息
    virtual uint16_t readAuxInfo(float& temperature) { return IL_OK; }

    // 进入BSL状态
    virtual uint16_t enterBSLMode() { return IL_OK; }

protected:
    virtual void onOpen() {}
    virtual void onClose() {}

    uint16_t invoke(OperationPtr operation, bool isAsync = false)
    {
        return _scheduler.invoke(operation, isAsync);
    }

private:
    Scheduler _scheduler;
};

typedef std::shared_ptr<Board> BoardPtr;

class INTERFACELAYER_API BoardStation
{
public:
    static BoardStation& instance();
    void update(int delta);

    bool open();
    void close();

    bool isOpened() const;

    BoardPtr device() { return _boardPtr; }

private:
    BoardPtr _boardPtr;
};