#pragma once

#include "Global.h"
#include "Operation.h"
#include "SerialPortImpl.h"

#define INSTRUCTION_DATA_OFFSET     2
#define RESPONSE_DATA_OFFSET        3

struct EncodedData
{
    uint8_t data[3];
};

class INTERFACELAYER_API SerialPortOpt : public Operation
{
public:
    SerialPortOpt(char instruction, int sendDataLen, SerialPortImpl* comHandle);
    virtual ~SerialPortOpt();

protected:
    // 分配缓存
    void allocateBuffer();

    // 发送指令和接收数据
    void sendAndReceive();

    // 解析接收到的数据
    virtual bool parseResponse() { return true; }

    // 获取字符串
    std::string extractString(uint16_t* buffer, int strLength);

private:
    // 发送指令
    bool sendInstruction();

    // 生成指令校验和
    void calcInstructionChecksum();

    // 编码
    void encoding(uint16_t* input, int length, EncodedData* output);

    // 解码
    void decoding(EncodedData* output, int length, uint16_t* input);

    // 校验接收到的数据
    bool verifyResponse(uint16_t* responseData, int length);

    // 交换高低字节
    void exchangeByte(uint16_t* buffer, int length);

    // 向日志发送十六进制指令
    void logInstruction();
    
    // 向日志发送十六进制接收到的数据
    void logResponse();

protected:
    // 指令代码
    char _instruction;

    // 发送数据长度
    int _sendDataLen;

    // 指令总长度
    int _instructionLen;

    uint16_t* _instructionBuffer;

    // 编码后发送的数据
    EncodedData* _encodingBuffer;

    // 接收到的数据
    EncodedData* _decodingBuffer;

    // 解码后接收到的数据
    uint16_t* _responseBuffer;

    // 接收到的数据总长度
    int _responseDataLen;

private:
    SerialPortImpl* _comHandle;

    // 是否向日志发送指令和数据
    bool _outputToLog;
};
