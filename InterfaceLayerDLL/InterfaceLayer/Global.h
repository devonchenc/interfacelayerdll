#pragma once

#pragma warning(disable:4251)

#define INSB_RSV                    0x00
#define INSB_READ_DEVICE_INFO       0x01
#define INSB_CHANGE_BAUDRATE        0x02
#define INSB_COM_TEST               0x03
#define INSB_READ_STATUS            0x04
#define INSB_CLEAR_ALL_ERROR        0x05
#define INSB_SYNC_TIME              0x06
#define INSB_READ_TIME              0x07
#define INSB_WRITE_PARA             0x08
#define INSB_READ_PARA              0x09
#define INSB_START_ACQUIRE          0x0A
#define INSB_READ_ACQUIRED_DATA     0x0B
#define INSB_WRITE_RAW_DATA         0x0C
#define INSB_READ_RAW_DATA          0x0D
#define INSB_ENTER_DVP_MODE         0x0E
#define INSB_EXIT_DVP_MODE          0x0F
#define INSB_TEMINATE_EXE           0x10

#define INSB_ENTER_BSL_MODE         0x28

#define INSB_READ_AUX_INFO          0x40

#ifdef _DEBUG
#define DEBUG_STDOUT(x) (std::cout << (x) << std::endl)
#define DEBUG_STDERR(x) (std::cerr << (x) << std::endl)
#else 
#define DEBUG_STDOUT(x)
#define DEBUG_STDERR(x)
#endif

#ifdef INTERFACELAYER_EXPORTS
#define INTERFACELAYER_API __declspec(dllexport)
#else
#define INTERFACELAYER_API __declspec(dllimport)
#endif

// 信息类型
enum IL_INFO_TYPE
{
    DeviceType = 1,     // 仪器型号
    HardwareVersion,    // 电路硬件版本号
    SoftwareVersion,    // 电路软件版本号
    RAMVersion,         // RAM版本号
};

enum IL_PARA_TYPE
{
    FIR = 1,
    CIC,
    TIMECORE,
};

enum IL_TIMECORE_TYPE
{
    TXS = 1,
    WAIT = 2,
    LOOPS = 3,
    LOOPE = 4,
    RXE = 5,
    OTL = 6,
    RXLP = 7,
    END = 15
};

enum IL_FPGA_STATE
{
    None = 0,
    Transmit = 1,
    Receive = 2,
    Transcive = 3,
    Wait = 4,
    Rdwrram = 5,
    Program = 6,
    Invalid
};

struct IL_DEVICE_STATUS
{
    IL_FPGA_STATE   State;
    bool            FPGACfg;        // true:FPGA配置成功, false:FPGA配置失败
    bool            FPGAPLL;        // true:FPGA PLL启动成功, false:PLL启动失败
    int             TopLoopNum;     // 大循环次数
};

// Error Codes
#define IL_OK                           0x0000
#define IL_BOARD_CLOSED                 0x0001      // 硬件关闭
#define IL_WRONG_INSTRUCTION_LEN        0x0100      // 参数长度不正确
#define IL_INSTRUCTION_FAILED           0x0200      // 指令执行失败
#define IL_WRONG_TYPE                   0x8000      // 错误类型
#define IL_WRITE_DATA_FAILED            0x8001      // 写入失败
#define IL_WRONG_SEND_DATA              0x8002      // 错误数据
#define IL_WRONG_RAW_ADDRESS            0x8003      // RAM地址无效
#define IL_READ_DATA_LENGTH_CALIBRATED  0x8004      // 读数据长度被校准
#define IL_WRONG_RECEIVED_CHECKSUM      0x8005      // 收到的数据错误
#define IL_ENTER_DVP_FAILED             0x8006      // 进入DVP开发模式失败
#define IL_EXIT_DVP_FAILED              0x8007      // 退出DVP开发模式失败
#define IL_UNSUPPORTED_AUX_INFO_TYPE    0x8008      // 不支持的辅助信息类型
#define IL_ENTER_BSL_FAILED             0x8009      // 进入BSL模式失败
#define IL_COM_TIMEOUT                  0xFFFF      // 串口超时

#include <string>

// 获取时间戳字符串
std::string INTERFACELAYER_API getDataTime(int seconds);