#pragma once

#include "Board.h"
#include "SerialPortImpl.h"

class DomesticBoard : public Board
{
public:
    DomesticBoard() {}
    virtual ~DomesticBoard() {}

    bool open() override;
    void close() override;

    bool isOpened() const { return _serialPort.isOpened(); }

    void update(int delta) override;

    uint16_t readDeviceInfo(IL_INFO_TYPE infoType, std::vector<std::string>& infoVector) override;

    uint16_t changeBaudrate(int baudrate) override;

    uint16_t comTest() override;

    uint16_t readStatus(IL_DEVICE_STATUS& deviceStatus) override;

    uint16_t clearAllError() override;

    uint16_t syncTime() override;

    uint16_t readTime(int& readTime, bool compare = false) override;

    uint16_t writeFIRParameter(const std::vector<int>& value) override;

    uint16_t writeCICParameter(uint16_t value) override;

    uint16_t writeTimeCoreParameter(int address, const std::vector<TimeCore>& parameters) override;

    uint16_t readParameter() override;

    uint16_t startAcquire() override;

    uint16_t readAcquiredData(int& readLen, uint16_t* dataBuffer) override;

    uint16_t writeRamData(uint64_t ramAddress, int writeLen, const uint16_t* dataBuffer) override;

    uint16_t readRamData(uint64_t ramAddress, int readLen, uint16_t* dataBuffer) override;

    uint16_t enterDVPMode() override;

    uint16_t exitDVPMode() override;

    uint16_t terminateExe() override;

    uint16_t readAuxInfo(float& temperature) override;

    uint16_t enterBSLMode() override;

protected:
    void onOpen() override {}
    void onClose() override {}

private:
    bool compareTime(int readTime);

private:
    SerialPortImpl _serialPort;
};