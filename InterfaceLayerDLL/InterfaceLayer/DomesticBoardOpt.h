#pragma once

#include <map>

#include "SerialPortOpt.h"

// 读取设备信息
class ReadDeviceInfoOpt : public SerialPortOpt
{
public:
    ReadDeviceInfoOpt(SerialPortImpl* comHandle, IL_INFO_TYPE infoType, std::vector<std::string>& infoVector);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    IL_INFO_TYPE _infoType;
    std::vector<std::string>& _infoVector;
};

// 改变设备波特率
class ChangeBaudrateOpt : public SerialPortOpt
{
public:
    ChangeBaudrateOpt(SerialPortImpl* comHandle, int baudrate);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    int _baudrate;
};

// 通讯测试
class ComTestOpt : public SerialPortOpt
{
public:
    ComTestOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 读取设备状态
class ReadStatusOpt : public SerialPortOpt
{
public:
    ReadStatusOpt(SerialPortImpl* comHandle, IL_DEVICE_STATUS& deviceStatus);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    uint16_t _statusWordHigh;
    uint16_t _statusWordLow;

    IL_DEVICE_STATUS& _deviceStatus;
};

// 清除设备错误状态
class ClearAllErrorOpt :public SerialPortOpt
{
public:
    ClearAllErrorOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
};

// 同步时间
class SyncTimeOpt : public SerialPortOpt
{
public:
    SyncTimeOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 读取设备时间
class ReadTimeOpt : public SerialPortOpt
{
public:
    ReadTimeOpt(SerialPortImpl* comHandle, int& readTime);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    int& _readTime;
};

// 写FIR参数
class WriteFIRParameterOpt : public SerialPortOpt
{
public:
    WriteFIRParameterOpt(SerialPortImpl* comHandle, const std::vector<int>& value);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    std::vector<int> _value;
};

// 写CIC参数
class WriteCICParameterOpt : public SerialPortOpt
{
public:
    WriteCICParameterOpt(SerialPortImpl* comHandle, uint16_t value);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    uint16_t _value;
};

typedef std::map<std::string, std::string> TimeCore;

// 写TimeCore参数
class WriteTimeCoreParameterOpt : public SerialPortOpt
{
public:
    WriteTimeCoreParameterOpt(SerialPortImpl* comHandle, int address, const std::vector<TimeCore>& parameters);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    bool translatedintoCode(const TimeCore& timeCore, uint16_t* buffer);

private:
    int _address;
    std::vector<TimeCore> _parameters;
};

// 读参数
class ReadParameterOpt : public SerialPortOpt
{
public:
    ReadParameterOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 开始采集
class StartAcquireOpt : public SerialPortOpt
{
public:
    StartAcquireOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 读取采集完成的数据
class ReadAcquiredDataOpt : public SerialPortOpt
{
public:
    ReadAcquiredDataOpt(SerialPortImpl* comHandle, int& readLen, uint16_t* dataBuffer);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    int& _readLen;
    uint16_t* _dataBuffer;
};

// 写数据至设备RAM
class WriteRamDataOpt : public SerialPortOpt
{
public:
    WriteRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int writeLen, const uint16_t* dataBuffer);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    uint64_t _ramAddress;
    int _writeLen;
    const uint16_t* _dataBuffer;
};

// 从设备RAM读取数据
class ReadRamDataOpt : public SerialPortOpt
{
public:
    ReadRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int readLen, uint16_t* dataBuffer);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    uint64_t _ramAddress;
    int _readLen;
    uint16_t* _dataBuffer;
};

// 进入开发模式
class EnterDVPModeOpt : public SerialPortOpt
{
public:
    EnterDVPModeOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 退出开发模式
class ExitDVPModeOpt : public SerialPortOpt
{
public:
    ExitDVPModeOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 强制停止设备一切工作
class TerminateExeOpt : public SerialPortOpt
{
public:
    TerminateExeOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};

// 读取设备辅助信息
class ReadAuxInfoOpt : public SerialPortOpt
{
public:
    ReadAuxInfoOpt(SerialPortImpl* comHandle, float& temperature);

protected:
    void onExecute() override;
    bool parseResponse() override;

private:
    float& _temperature;
};

// 进入BSL状态
class EnterBSLModeOpt : public SerialPortOpt
{
public:
    EnterBSLModeOpt(SerialPortImpl* comHandle);

protected:
    void onExecute() override;
    bool parseResponse() override;
};