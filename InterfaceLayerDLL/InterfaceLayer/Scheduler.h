#pragma once

#include "SafeQueue.h"
#include "Operation.h"

#include <thread>

class Scheduler
{
public:
    Scheduler();

    void keepAliveOpt(OperationPtr operation);

    // 调度任务，默认为同步调用
    uint16_t invoke(OperationPtr operation, bool isAsync = false);

    void start();

    void stop();

    bool isRunning() const { return _isRunning; }

    bool isBusy() { return !_executeQueue.empty(); }

    void update();

    void cancel() { _executeQueue.clear(); }

    void setSleepTime(int time) { _sleepTime = time; }

private:
    void _run();

private:
    SafeQueue<OperationPtr> _executeQueue;
    SafeQueue<OperationPtr> _replyQueue;

    OperationPtr _keepAliveOpt;

    std::thread _thread;

    bool _isRunning;

    // 单位毫秒
    int _sleepTime;
};