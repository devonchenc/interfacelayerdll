﻿#include <iostream>

#include "../../InterfaceLayerDLL/InterfaceLayer/Board.h"

#ifdef _DEBUG
#pragma comment(lib, "../../InterfaceLayerDLL/x64/Debug/InterfaceLayerD.lib")
#else
#pragma comment(lib, "../../InterfaceLayerDLL/x64/Release/InterfaceLayer.lib")
#endif

int main()
{
    // 连接设备
    if (!BoardStation::instance().open())
    {
        DEBUG_STDOUT("Connect = Fail");
        return 1;
    }

    // 返回Board智能指针
    BoardPtr board = BoardStation::instance().device();
    if (!board)
    {
        DEBUG_STDOUT("Connect = Fail");
        return 1;
    }

    // 读取设备信息
    IL_INFO_TYPE infoType = HardwareVersion;
    std::vector<std::string> infoVector;
    uint16_t result = board->readDeviceInfo(infoType, infoVector);
    if (result == IL_OK)
    {
        for (int i = 0; i < infoVector.size(); i++)
        {
            std::cout << infoVector[i] << std::endl;
        }
    }

    // 串口测试
    result = board->comTest();
    if (result == IL_OK)
    {
        std::cout << "Com Test Success" << std::endl;
    }

    // 读取设备当前状态
    IL_DEVICE_STATUS deviceStatus;
    result = board->readStatus(deviceStatus);
    if (result == IL_OK)
    {
        std::cout << "State: " << deviceStatus.State << std::endl;
        std::cout << "FPGACfg: " << deviceStatus.FPGACfg << std::endl;
        std::cout << "FPGAPLL: " << deviceStatus.FPGAPLL << std::endl;
        std::cout << "TopLoopNum: " << deviceStatus.TopLoopNum << std::endl;
    }

    // 读取设备辅助信息
    float temperature;
    result = board->readAuxInfo(temperature);
    if (result == IL_OK)
    {
        std::cout << "Device Temperature: " << temperature << std::endl;
    }

    // 同步时间至设备中
    result = board->syncTime();
    if (result == IL_OK)
    {
        int deviceTime;
        board->readTime(deviceTime);
        std::cout << "Device Time: " << getDataTime(deviceTime) << std::endl;
    }

    // 向设备RAM指定位置写入数据
    const int length = 2;
    uint16_t writeData[length];
    writeData[0] = 123;
    writeData[1] = 456;
    result = board->writeRamData(0, length, writeData);
    if (result == IL_OK)
    {
        uint16_t readData[2];
        result = board->readRamData(0, length, readData);
        if (result == IL_OK)
        {
            for (int i = 0; i < length; i++)
            {
                std::cout << "Read RAM data " << i << ": " << readData[i] << std::endl;
            }
        }
    }

    // 进入DVP开发模式
/*    result = board->enterDVPMode();
    if (result == IL_OK)
    {
        // 退出DVP开发模式
        result = board->exitDVPMode();
    }*/

    // 关闭设备
    BoardStation::instance().close();
}
